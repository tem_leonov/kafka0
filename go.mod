module gitlab.com/tem_leonov/kafka0

go 1.18

require (
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/segmentio/kafka-go v0.4.30
)

require (
	github.com/klauspost/compress v1.14.2 // indirect
	github.com/pierrec/lz4/v4 v4.1.14 // indirect
)
