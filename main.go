package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"github.com/google/uuid"

	"gitlab.com/tem_leonov/kafka0/internal/app/coordinator"
	"gitlab.com/tem_leonov/kafka0/internal/app/transport"
	"gitlab.com/tem_leonov/kafka0/internal/pkg/disabler"
)

type filters []string

func (f *filters) String() string {
	return "query for filtering messages"
}

func (f *filters) Set(s string) error {
	*f = append(*f, s)
	return nil
}

func main() {
	log.SetOutput(os.Stdout)

	ctx := context.Background()
	osSigCh := make(chan os.Signal, 1)
	signal.Notify(osSigCh, syscall.SIGINT, syscall.SIGTERM)

	from := flag.String(
		"s", "localhost:9092", "source (e.g. kafka::localhost:9092::src_topic, localhost:9092::src_topic)",
	)
	to := flag.String(
		"d", "localhost:9092", "destination (e.g. kafka::localhost:9092::dest_topic, localhost:9092::dest_topic, std)",
	)
	var f filters
	flag.Var(&f, "f", "query for filtering messages")
	msgLimit := flag.Uint("l", 0, "maximum of transmitted messages, 0 for unlimited (default 0)")
	flag.Parse()

	flag.Usage = func() {
		fmt.Fprintf(
			flag.CommandLine.Output(),
			"Usage: kafka0 -s sourcekafka.com -d destkafka.com -t source_topic:dest_topic -l 0\n",
		)
		flag.PrintDefaults()
	}
	if flag.NFlag() == 0 {
		flag.Usage()
		return
	}

	if from == nil || len(*from) == 0 {
		log.Fatal("Source connection string is empty")
	}
	if to == nil || len(*to) == 0 {
		log.Fatal("Destination connection string is empty")
	}

	var wg sync.WaitGroup
	sessionID := uuid.New()
	ctx = disabler.StartOS(ctx, osSigCh)
	srcType, srcParams := parseSrcConnStr(*from)
	dstType, dstParams := parseDstConnStr(*to)
	key, value := parseFilters(f)
	err := coordinator.Start(ctx, &wg, sessionID, srcType, srcParams, dstType, dstParams, key, value, *msgLimit)
	if err != nil {
		log.Fatalf("error starting coordinator: %s", err)
	}
	wg.Wait()
}

func parseFilters(filters []string) (string, string) {
	var key, val string
	for _, filter := range filters {
		typ, q, _ := strings.Cut(filter, "::")
		if typ == "key" {
			key = q
			continue
		}
		if typ == "value" {
			val = q
		}
	}

	return key, val
}

func parseSrcConnStr(connStr string) (transport.InType, []string) {
	params := strings.Split(connStr, "::")
	typ := transport.DefaultIn
	if _, ok := transport.InTypes[params[0]]; ok {
		typ = transport.InType(params[0])
		params = params[1:]
	}
	return typ, params
}

func parseDstConnStr(connStr string) (transport.OutType, []string) {
	params := strings.Split(connStr, "::")
	typ := transport.DefaultOut
	if _, ok := transport.OutTypes[params[0]]; ok {
		typ = transport.OutType(params[0])
		params = params[1:]
	}
	return typ, params
}
