package logger

import "log"

type LogLogger struct {
	prefix string
}

func (l LogLogger) Printf(s string, i ...interface{}) {
	format := "%s " + s
	v := make([]interface{}, 0, len(i)+1)
	v = append(v, l.prefix)
	v = append(v, i...)
	log.Printf(format, v)
}
