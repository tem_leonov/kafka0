package disabler

import (
	"context"
	"log"
	"os"
)

func StartOS(ctx context.Context, osSigCh <-chan os.Signal) context.Context {
	ctx, cancel := context.WithCancel(ctx)
	go osLoop(ctx, cancel, osSigCh, "OS signal disabler")
	return ctx
}

func osLoop(ctx context.Context, stop context.CancelFunc, osSigCh <-chan os.Signal, name string) {
	defer log.Printf("%s shutdown", name)
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case <-osSigCh:
			log.Printf("%s received termination signal from OS", name)
			stop()
			return
		}
	}
}
