package filter

import (
	"context"
	"log"
	"strings"
	"sync"

	"gitlab.com/tem_leonov/kafka0/internal/app/domain"
)

const name = "[filter]"

func Start(
	ctx context.Context, wg *sync.WaitGroup, inCh chan *domain.Message, keyFilter string, valFilter string,
) chan *domain.Message {
	if keyFilter == "" && valFilter == "" {
		return inCh
	}

	outCh := make(chan *domain.Message, cap(inCh))
	wg.Add(1)
	go func() {
		defer func() {
			log.Printf("%s shutdown", name)
			wg.Done()
		}()
		loop(ctx, inCh, outCh, keyFilter, valFilter)
	}()
	return outCh
}

func loop(
	ctx context.Context, inCh <-chan *domain.Message, outCh chan<- *domain.Message, keyFilter string, valFilter string,
) {
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case m, ok := <-inCh:
			if !ok {
				log.Printf("%s input channel closed", name)
				return
			}

			if keyFilter != "" && !strings.EqualFold(string(m.Key), keyFilter) {
				continue
			}
			if valFilter != "" && !strings.Contains(strings.ToLower(string(m.Value)), strings.ToLower(valFilter)) {
				continue
			}

			select {
			case <-ctx.Done():
				continue
			case outCh <- m:
			}
		}
	}
}
