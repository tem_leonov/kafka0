package domain

import (
	"time"

	"github.com/segmentio/kafka-go"
)

type Message struct {
	HighWaterMark int64          `json:"high_water_mark"`
	Key           []byte         `json:"key"`
	Value         []byte         `json:"value"`
	Headers       []kafka.Header `json:"headers"`
	Time          time.Time      `json:"time"`
}
