package limiter

import (
	"context"
	"log"
	"sync"
)

const name = "[limiter]"

func Start[T any](ctx context.Context, wg *sync.WaitGroup, inCh chan T, limit uint, cancel context.CancelFunc) chan T {
	if limit == 0 {
		return inCh
	}

	outCh := make(chan T, cap(inCh))
	wg.Add(1)
	go func() {
		defer func() {
			log.Printf("%s shutdown", name)
			wg.Done()
		}()
		loop(ctx, inCh, outCh, limit, cancel)
	}()
	return outCh
}

func loop[T any](ctx context.Context, inCh <-chan T, outCh chan<- T, limit uint, cancel context.CancelFunc) {
	defer cancel()
	var processed uint
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case m, ok := <-inCh:
			if !ok {
				log.Printf("%s input channel closed", name)
				return
			}
			outCh <- m
			processed++
			if processed >= limit {
				log.Printf("%s messages limit reached", name)
				return
			}
		}
	}
}
