package coordinator

import (
	"context"
	"fmt"
	"sync"

	"github.com/google/uuid"

	"github.com/pkg/errors"

	"gitlab.com/tem_leonov/kafka0/internal/app/domain"
	"gitlab.com/tem_leonov/kafka0/internal/app/filter"
	"gitlab.com/tem_leonov/kafka0/internal/app/limiter"
	"gitlab.com/tem_leonov/kafka0/internal/app/measurer"
	"gitlab.com/tem_leonov/kafka0/internal/app/transport"
)

const name = "[coordinator]"

func Start(
	ctx context.Context, wg *sync.WaitGroup, sessionID uuid.UUID,
	inType transport.InType, inParams []string,
	outType transport.OutType, outParams []string,
	keyFilter, valueFilter string, limit uint,
) error {
	ctx, cancel := context.WithCancel(ctx)
	measurer.Start(ctx)
	srcOutCh := make(chan *domain.Message)
	dstInCh := filter.Start(ctx, wg, srcOutCh, keyFilter, valueFilter)
	dstInCh = limiter.Start(ctx, wg, dstInCh, limit, cancel)

	err := startSrc(ctx, wg, sessionID, srcOutCh, inType, inParams)
	if err != nil {
		return errors.Wrap(err, "starting input transport")
	}

	err = startDst(ctx, wg, dstInCh, outType, outParams)
	if err != nil {
		return errors.Wrap(err, "starting output transport")
	}
	return nil
}

func startSrc(
	ctx context.Context, wg *sync.WaitGroup, sessionID uuid.UUID,
	outCh chan<- *domain.Message, typ transport.InType, params []string,
) error {
	switch typ {
	case transport.KafkaIn:
		err := validateParams(params, 2)
		if err != nil {
			return errors.Wrapf(err, "validating params for transport %s", typ)
		}
		cg := fmt.Sprintf("kafka0.%s", sessionID)
		transport.StartKafkaIn(ctx, wg, outCh, params[0], params[1], cg)
	case transport.JsonIn:
		err := validateParams(params, 1)
		if err != nil {
			return errors.Wrapf(err, "validating params for transport %s", typ)
		}
		transport.StartJsonIn(ctx, wg, outCh, params[0])
	default:
		return fmt.Errorf("unknown transport %s", typ)
	}
	return nil
}

func startDst(
	ctx context.Context, wg *sync.WaitGroup, inCh <-chan *domain.Message, typ transport.OutType,
	params []string,
) error {
	switch typ {
	case transport.KafkaOut:
		err := validateParams(params, 2)
		if err != nil {
			return errors.Wrapf(err, "validating params for transport %s", typ)
		}
		transport.StartKafkaOut(ctx, wg, inCh, params[0], params[1])
	case transport.StdOut:
		err := validateParams(params, 0)
		if err != nil {
			return errors.Wrapf(err, "validating params for transport %s", typ)
		}
		transport.StartStdOut(ctx, wg, inCh)
	case transport.JsonOut:
		err := validateParams(params, 1)
		if err != nil {
			return errors.Wrapf(err, "validating params for transport %s", typ)
		}
		transport.StartJsonOut(ctx, wg, inCh, params[0])
	default:
		return fmt.Errorf("unknown transport %s", typ)
	}
	return nil
}

func validateParams(params []string, expectedCount uint) error {
	if uint(len(params)) != expectedCount {
		return fmt.Errorf("expected %d params, got %d", expectedCount, len(params))
	}
	return nil
}
