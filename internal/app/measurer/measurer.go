package measurer

import (
	"context"
	"log"
	"time"

	"gitlab.com/tem_leonov/kafka0/internal/app/metrics"
)

const name = "[measurer]"

func Start(ctx context.Context) {
	go loop(ctx)
}

func loop(ctx context.Context) {
	var (
		successMsgs, totalMsgs         uint
		prevSuccessMsgs, prevTotalMsgs uint
	)
	t := time.NewTicker(10 * time.Second)
	defer t.Stop()
	defer log.Printf("%s shutdown", name)
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case <-t.C:
			prevSuccessMsgs = successMsgs
			prevTotalMsgs = totalMsgs
			successMsgs = metrics.TotalMsgs() - metrics.ErrMsgs()
			totalMsgs = metrics.TotalMsgs()
			log.Printf(
				"%s Delivered messages. Success: %d (%+d). Total: %d (%+d)", name, successMsgs,
				successMsgs-prevSuccessMsgs, totalMsgs, totalMsgs-prevTotalMsgs,
			)
		}
	}
}
