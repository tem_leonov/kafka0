package transport

import (
	"context"
	"log"
	"sync"

	"gitlab.com/tem_leonov/kafka0/internal/app/domain"
	"gitlab.com/tem_leonov/kafka0/internal/app/metrics"
)

func StartStdOut(ctx context.Context, wg *sync.WaitGroup, inCh <-chan *domain.Message) {
	wg.Add(1)
	go func() {
		const name = "[transport:std_out]"
		defer func() {
			log.Printf("%s shutdown", name)
			wg.Done()
		}()
		loop(ctx, name, inCh)
	}()
}

func loop(ctx context.Context, name string, inCh <-chan *domain.Message) {
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case msg, ok := <-inCh:
			if !ok {
				log.Printf("%s input channel closed", name)
				return
			}
			log.Printf("Message: %s\n", string(msg.Value))
			metrics.CountMessage(true)
		}
	}
}
