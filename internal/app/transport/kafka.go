package transport

import (
	"context"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/segmentio/kafka-go"

	"gitlab.com/tem_leonov/kafka0/internal/app/domain"
	"gitlab.com/tem_leonov/kafka0/internal/app/metrics"
)

type logLogger struct {
	prefix string
}

func (l logLogger) Printf(s string, i ...interface{}) {
	format := "%s " + s
	v := make([]interface{}, 0, len(i)+1)
	v = append(v, l.prefix)
	v = append(v, i...)
	log.Printf(format, v)
}

func StartKafkaIn(
	ctx context.Context, wg *sync.WaitGroup, outCh chan<- *domain.Message, connStr, topic, cg string,
) {
	brokers := parseKafkaBrokers(connStr)
	logger := logLogger{"[kafka_consumer]"}
	consumer := kafka.NewReader(
		kafka.ReaderConfig{
			Brokers:     brokers,
			GroupID:     cg,
			Topic:       topic,
			MaxBytes:    10e7, // 100MB
			StartOffset: kafka.LastOffset,
			ErrorLogger: logger,
		},
	)

	wg.Add(1)
	go func() {
		const name = "[transport:kafka_in]"
		defer func() {
			log.Printf("%s shutdown", name)
			err := consumer.Close()
			if err != nil {
				log.Printf("%s error closing consumer: %s", name, err)
			}
			close(outCh)

			wg.Done()
		}()

		kafkaInLoop(ctx, name, outCh, consumer)
	}()
}

func kafkaInLoop(ctx context.Context, name string, outCh chan<- *domain.Message, reader *kafka.Reader) {
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		default:
			msg, err := reader.FetchMessage(ctx)
			if err != nil {
				log.Printf("%s fetching message: %s", name, err)
				continue
			}
			select {
			case <-ctx.Done():
				continue
			case outCh <- mapMsg(msg):
			}
		}
	}
}

func mapMsg(msg kafka.Message) *domain.Message {
	return &domain.Message{
		HighWaterMark: msg.HighWaterMark,
		Key:           msg.Key,
		Value:         msg.Value,
		Headers:       msg.Headers,
		Time:          msg.Time,
	}
}

func StartKafkaOut(ctx context.Context, wg *sync.WaitGroup, inCh <-chan *domain.Message, connStr, topic string) {
	brokers := parseKafkaBrokers(connStr)
	logger := logLogger{"[kafka_producer]"}
	producer := kafka.Writer{
		Addr:        kafka.TCP(brokers...),
		Topic:       topic,
		Balancer:    &kafka.Hash{},
		BatchSize:   1,
		ErrorLogger: logger,
	}
	wg.Add(1)
	go func() {
		const name = "[transport:kafka_out]"
		defer func() {
			log.Printf("%s shutdown", name)

			err := producer.Close()
			if err != nil {
				log.Printf("%s error closing producer: %s", name, err)
			}
			wg.Done()
		}()
		kafkaOutLoop(ctx, name, inCh, &producer)
	}()
}

func kafkaOutLoop(ctx context.Context, name string, inCh <-chan *domain.Message, writer *kafka.Writer) {
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case msg, ok := <-inCh:
			if !ok {
				log.Printf("%s input channel closed", name)
				return
			}
			err := writer.WriteMessages(
				context.Background(), kafka.Message{
					HighWaterMark: msg.HighWaterMark,
					Key:           msg.Key,
					Value:         msg.Value,
					Headers:       msg.Headers,
					Time:          time.Now(),
				},
			)
			if err != nil {
				log.Printf("%s error publishing message, %s", name, err)
			}
			metrics.CountMessage(err == nil)
		}
	}
}

func parseKafkaBrokers(connStr string) []string {
	return strings.Split(connStr, ",")
}
