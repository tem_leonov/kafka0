package transport

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/tem_leonov/kafka0/internal/app/domain"
)

func StartJsonIn(ctx context.Context, wg *sync.WaitGroup, outCh chan<- *domain.Message, path string) error {
	f, err := os.Open(path)
	if err != nil {
		return errors.Wrap(err, "opening file")
	}

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return errors.Wrap(err, "reading file")
	}

	var msgs []*domain.Message
	err = json.Unmarshal(b, &msgs)
	if err != nil {
		return errors.Wrap(err, "unmarshalling data")
	}

	wg.Add(1)
	go func() {
		const name = "[json:in]"
		defer func() {
			log.Printf("%s shutdown", name)
			close(outCh)
			wg.Done()
		}()
		sendMessagesIntoCh(ctx, name, outCh, msgs)
	}()
	return nil
}

func sendMessagesIntoCh(ctx context.Context, name string, outCh chan<- *domain.Message, msgs []*domain.Message) {
	for _, msg := range msgs {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			return
		case outCh <- msg:
		}
	}
}

func StartJsonOut(ctx context.Context, wg *sync.WaitGroup, inCh <-chan *domain.Message, path string) error {
	wg.Add(1)
	go func() {
		const name = "[json:out]"
		defer func() {
			log.Printf("%s shutdown", name)
			wg.Done()
		}()
		jsonOutLoop(ctx, name, inCh, path)
	}()
	return nil
}

func jsonOutLoop(ctx context.Context, name string, inCh <-chan *domain.Message, path string) {
	const (
		batchSize   = 100
		maxInterval = 1 * time.Minute
	)
	saved := 0
	buf := make([]*domain.Message, 0, batchSize)
	dump := false
	t := time.NewTicker(maxInterval)

loop:
	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", name)
			break loop
		case msg, ok := <-inCh:
			if !ok {
				log.Printf("%s input channel closed", name)
				break loop
			}

			buf = append(buf, msg)
			if len(buf) >= saved+batchSize {
				dump = true
			}
		case <-t.C:
			dump = true
		default:
			if !dump {
				continue
			}
			dump = false

			if len(buf) == saved {
				continue
			}

			err := saveMessagesIntoFile(path, buf)
			if err != nil {
				log.Printf("%s error dumping to file: %s", name, err)
				continue
			}
			saved = len(buf)
		}
	}

	if len(buf) == saved {
		return
	}

	err := saveMessagesIntoFile(path, buf)
	if err != nil {
		log.Printf("%s error dumping to file: %s", name, err)
	}
}

func saveMessagesIntoFile(path string, msgs []*domain.Message) error {
	j, err := json.Marshal(msgs)
	if err != nil {
		return errors.Wrap(err, "marshalling messages")
	}

	err = ioutil.WriteFile(path, j, 0644)
	if err != nil {
		return errors.Wrap(err, "writing to file")
	}
	return nil
}
