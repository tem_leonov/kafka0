package transport

type typ string

const (
	kafkaType typ = "kafka"
	stdType   typ = "std"
	jsonType      = "json"
)

type InType typ

const (
	DefaultIn = InType(kafkaType)
	KafkaIn   = InType(kafkaType)
	JsonIn    = InType(jsonType)
)

var InTypes = map[string]struct{}{
	KafkaIn.String(): {},
	JsonIn.String():  {},
}

func (t InType) String() string {
	return string(t)
}

type OutType typ

const (
	DefaultOut = OutType(kafkaType)
	KafkaOut   = OutType(kafkaType)
	StdOut     = OutType(stdType)
	JsonOut    = OutType(jsonType)
)

var OutTypes = map[string]struct{}{
	KafkaOut.String(): {},
	StdOut.String():   {},
	JsonOut.String():  {},
}

func (t OutType) String() string {
	return string(t)
}
