package metrics

import "sync"

type Metrics struct {
	totalMsgs uint
	errMsgs   uint
}

var (
	m   = Metrics{}
	mut = sync.RWMutex{}
)

func CountMessage(successful bool) {
	mut.Lock()
	defer mut.Unlock()
	m.totalMsgs++
	if !successful {
		m.errMsgs++
	}
}

func TotalMsgs() uint {
	mut.RLock()
	defer mut.RUnlock()
	return m.totalMsgs
}

func ErrMsgs() uint {
	mut.RLock()
	defer mut.RUnlock()
	return m.errMsgs
}
